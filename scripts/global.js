const burger = document.querySelector("div.burger");

burger.onclick = openList;

function openList() {
  burger.classList.toggle("toggle");
  var list = document.querySelector("nav.navbar ul");
  list.classList.toggle("toggle");
}
