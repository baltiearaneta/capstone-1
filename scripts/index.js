const track = document.querySelector(".carousel-track");
console.dir(track);
const slides = Array.from(track.children);
console.dir(slides);
const nextButton = document.querySelector(".carousel-button-right");
console.dir(nextButton);
const prevButton = document.querySelector(".carousel-button-left");
console.dir(prevButton);
const barNav = document.querySelector(".carousel-nav");
console.dir(barNav);
const navBarButtons = Array.from(barNav.children);
console.dir(navBarButtons);
const slideWidth = slides[0].getBoundingClientRect().width;
console.log(slideWidth);

function setSlidePosition(slide, index) {
  slide.style.left = slideWidth * index + "px";
}

slides.forEach(setSlidePosition);

const moveToSlide = (track, currentSlide, targetSlide) => {
  track.style.transform = "translateX(-" + targetSlide.style.left + ")";
  currentSlide.classList.remove("current-slide");
  targetSlide.classList.add("current-slide");
};

const updateBars = (currentBar, targetBar) => {
  currentBar.classList.remove("current-slide");
  targetBar.classList.add("current-slide");
};

prevButton.addEventListener("click", (e) => {
  const currentSlide = track.querySelector(".current-slide");
  const prevSlide = currentSlide.previousElementSibling;
  moveToSlide(track, currentSlide, prevSlide);
  const currentBar = barNav.querySelector(".current-slide");
  const prevBar = currentBar.previousElementSibling;
  updateBars(currentBar, prevBar);
});

nextButton.addEventListener("click", (e) => {
  const currentSlide = track.querySelector(".current-slide");
  const nextSlide = currentSlide.nextElementSibling;
  moveToSlide(track, currentSlide, nextSlide);
  const currentBar = barNav.querySelector(".current-slide");
  const nextBar = currentBar.nextElementSibling;
  updateBars(currentBar, nextBar);
});

barNav.addEventListener("click", (e) => {
  const targetBar = e.target.closest("button");
  if (!targetBar) return;
  console.log(targetBar);
  const currentSlide = track.querySelector(".current-slide");
  const currentBar = barNav.querySelector(".current-slide");
  const targetIndex = navBarButtons.findIndex((bar) => bar === targetBar);
  console.log(targetIndex);
  const targetSlide = slides[targetIndex];
  moveToSlide(track, currentSlide, targetSlide);
  updateBars(currentBar, targetBar);
});
